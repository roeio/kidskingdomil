# -*- coding: utf-8 -*-
from bs4 import BeautifulSoup
import constants
import urllib2
import xbmc
import sys
import urllib
import xbmcgui
import xbmcplugin

def create_connection(url):
	response = urllib2.urlopen(url)
	page = response.read()
	return page

def extract_title_and_link(page_response, name=None):
	video_information = {}
	#bs = BeautifulSoup(page_response, 'html.parser')
	bs = BeautifulSoup(page_response, 'html.parser', from_encoding="utf-8")
	return extract_data_from_response(bs, name)

#    for elm in bs.find_all(('h3', {'class':'yt-uix-sessionlink yt-uix-tile-link yt-ui-ellipsis yt-ui-ellipsis-2       spf-link a"'})):
#        for attr in elm.find_all('a', href=True):
#            video_information[attr.attrs['title']] = attr.attrs['href']
#    return video_information


def extract_data_from_response(page_response, name=None):
	video_information = {}
	playlists = {}
	#bs = BeautifulSoup(page_response, 'html.parser')
	for elm in page_response.find_all(('h3', {'class':'yt-uix-sessionlink yt-uix-tile-link yt-ui-ellipsis yt-ui-ellipsis-2       spf-link a"'})):
		for attr in elm.find_all('a', href=True):
			if 'list' not in attr.attrs['href']:
				if 'val' in attr.attrs['href'] or 'http' in attr.attrs['href']:
					pass
				elif len(attr.attrs['href'][9:]) > 12:
					pass
				else:
					x = attr.attrs['title']
					x = x.encode('UTF-8')
					video_information[x] = attr.attrs['href'][9:]
			elif 'list' in attr.attrs['href']:
				title = attr.attrs['title']
				title = title.encode('UTF-8')
				playlists[title] = attr.attrs['href'][26:]
				xbmc.log('Attrs output: {}'.format(attr.attrs))
	return video_information, playlists

def check_amount_of_pages(page_response=None):
	list = []
	#bs = BeautifulSoup(page_response, 'html.parser')
	bs = BeautifulSoup(page_response, 'html.parser', from_encoding="utf-8")
	for a in bs.find_all('a', {'class': 'yt-uix-button'}):
		xbmc.log('This is a test: {}'.format(a.attrs['href']))
		# 'search_query' in a.attrs['href'] and
		if 'results?' in a.attrs['href']:
			xbmc.log('Hereeeeee')
			list.append(a.attrs['href'])
	xbmc.log('This is the list: {}'.format(list))
	return list

def addDir3(name, url, mode, iconimage, number_of_items, pasta=True):
	u = sys.argv[0] + "?url=" + urllib.quote_plus(url) + "&mode=" + str(mode) + "&name=" + urllib.quote_plus(
		name) + "&iconimage=" + urllib.quote_plus(iconimage)
	ok = True
	liz = xbmcgui.ListItem(name)
	liz.setInfo(type="Video", infoLabels={"Title": name})
	# liz.setArt({ 'thumb': iconimage, 'banner' : os.path.join(artfolder,'banner.png'), 'fanart': os.path.join(addonfolder,'fanart.jpg') })
	liz.setPath(u)
	ok = xbmcplugin.addDirectoryItem(handle=int(sys.argv[1]), url=u, listitem=liz, isFolder=pasta,
									 totalItems=number_of_items)
	return ok

def extract_title_and_link_from_all_pages(page_response, name=None):
	video_information = []
	#bs = BeautifulSoup(page_response, 'html.parser')
	amountofpages = len(check_amount_of_pages(page_response))
	xbmc.log('Amount of pages: {}'.format(amountofpages))
	if len(check_amount_of_pages(page_response)) > 1:
		addDir3('Next Page2 >>', '', 5, '', 10)
		for page in check_amount_of_pages(page_response):
			next_page = create_connection(constants.youtube + page)
			bs = BeautifulSoup(next_page, 'html.parser')
			p = extract_data_from_response(bs, name)
			video_information.append(p)
	return video_information

def extract_title_and_link_from_next_page(page_response, name=None):
	video_information = []
	#bs = BeautifulSoup(page_response, 'html.parser')
	amountofpages = len(check_amount_of_pages(page_response))
	xbmc.log('Amount of pages: {}'.format(amountofpages))
	if len(check_amount_of_pages(page_response)) > 1:
		# addDir3('Next Page >>', '', 5, '', 10)
		for page in check_amount_of_pages(page_response):
			next_page = create_connection(constants.youtube + page)
			bs = BeautifulSoup(next_page, 'html.parser')
			p = extract_data_from_response(bs, name)
			video_information.append(p)
	return video_information
