# -*- coding: utf-8 -*-
import xbmcplugin
import xbmcgui
import xbmcaddon
import xbmc
import urllib
import re
import sys
import unicodedata

from lib import video
from lib import constants

url = "plugin://plugin.video.youtube/play/?video_id={0}"
mode = None


# videos = video.extract_title_and_link(constants.search_string)


# def addDir(name,url,mode,iconimage,page,number_of_items,token,pasta=True):
# 	u=sys.argv[0]+"?url="+urllib.quote_plus(url)+"&mode="+str(mode)+"&name="+urllib.quote_plus(name)+"&page="+str(page)+"&token="+urllib.quote_plus(token)
# 	ok=True
# 	liz=xbmcgui.ListItem(name)
# 	liz.setInfo( type="Video", infoLabels={ "Title": name })
# 	liz.setArt({ 'thumb': iconimage, 'banner' : os.path.join(artfolder,'banner.png'), 'fanart': os.path.join(addonfolder,'fanart.jpg') })
# 	liz.setPath(u)
# 	ok=xbmcplugin.addDirectoryItem(handle=int(sys.argv[1]),url=u,listitem=liz,isFolder=pasta,totalItems=number_of_items)
# 	return ok

def addDir3(name, url, mode, iconimage, number_of_items, dname='', pagenum=None, pasta=True):
	u = sys.argv[0] + "?url=" + urllib.quote_plus(url) + "&mode=" + str(mode) + "&name=" + urllib.quote_plus(
		name) + "&iconimage=" + urllib.quote_plus(iconimage) + "&pagenum=" + str(pagenum) + "&dname=" + dname
	ok = True
	liz = xbmcgui.ListItem(name, iconImage="DefaultFolder.png", thumbnailImage=iconimage)
	liz.setInfo(type="Video", infoLabels={"Title": name})
	# liz.setArt({ 'thumb': iconimage, 'banner' : os.path.join(artfolder,'banner.png'), 'fanart': os.path.join(addonfolder,'fanart.jpg') })
	liz.setPath(u)
	ok = xbmcplugin.addDirectoryItem(handle=int(sys.argv[1]), url=u, listitem=liz, isFolder=pasta,
									 totalItems=number_of_items)
	return ok


def addDir(name, url, mode, iconimage, description, pagenum=''):
	u = sys.argv[0] + "?url=" + urllib.quote_plus(url) + "&mode=" + str(mode) + "&name=" + urllib.quote_plus(
		name) + "&iconimage=" + urllib.quote_plus(iconimage) + "&description=" + urllib.quote_plus(description) \
		+ "&pagenum=" + str(pagenum)
	# u = sys.argv[0]
	ok = True
	liz = xbmcgui.ListItem(name, iconImage="DefaultFolder.png", thumbnailImage=iconimage)
	liz.setInfo(type="Video", infoLabels={"Title": name, "Plot": description})
	if mode == 1:
		ok = xbmcplugin.addDirectoryItem(handle=int(sys.argv[1]), url=u, listitem=liz, isFolder=True)
	elif mode == 3 or mode == 2 or mode == 5 or mode == 7:
		liz.setProperty("IsPlayable", "true")
		ok = xbmcplugin.addDirectoryItem(handle=int(sys.argv[1]), url=url, listitem=liz, isFolder=False, totalItems=3)
	else:
		ok = xbmcplugin.addDirectoryItem(handle=int(sys.argv[1]), url=u, listitem=liz, isFolder=True)
	return ok


def get_params():
	param_string = sys.argv[2]

	commands = {}

	if param_string:
		split_commands = param_string[param_string.find('?') + 1:].split('&')

		for command in split_commands:
			if len(command) > 0:
				if "=" in command:
					split_command = command.split('=')
					key = split_command[0]
					value = urllib.unquote_plus(split_command[1])
					commands[key] = value
				else:
					commands[command] = ""
	return commands


params = get_params()
mode = params.get("mode")
pagenum = params.get("pagenum")
try:
	dname = params.get("dname")
except:
	pass
try:
	name = params.get('name')
except:
	name = params.get('name'.decode('UTF-8'))
try:
	mode = int(params["mode"])
except:
	try:
		mode = params["mode"]
	except:
		pass
play_url = params.get("url")


def build_video_list(name):
	xbmc.log('Inside build video list, name is {}'.format(name))
	full_path = constants.search_string.format(urllib.quote(name))
	xbmc.log('Full path is: {}'.format(full_path))
	page = video.create_connection(full_path)
	# my_videos = video.extract_title_and_link(page, name)
	if len(video.check_amount_of_pages(page)) > 1:
		my_videos, my_playlists = video.extract_title_and_link(page, name)
		for v in my_videos:
			vname = v
			# xbmc.log('This is the lenv: {}'.format(lenv))
			# xbmc.log('This is v: {}'.format(my_videos[lenv]))
			addDir(vname, url.format(my_videos[v]), 3, '', vname, pagenum=1)
		if my_playlists:
			for p in my_playlists:
				xbmc.log('My playlist is {}'.format(my_playlists))
				addDir3(p + '(רשימת השמעה)',
						'plugin://plugin.video.youtube/play/?playlist_id={}'.format(my_playlists[p]),
						3, '', 10)
		addDir3('Next Page {} / {}>>'.format(1 ,len(video.check_amount_of_pages(page))), '', 5, '', 10, dname=params.get("name") ,pagenum=1)
	# my_videos = video.extract_title_and_link_from_all_pages(page,name)
	# for lenv in xrange(len(my_videos)):
	#     for v in my_videos[lenv]:
	#         vname = v
	#         xbmc.log('This is the lenv: {}'.format(lenv))
	#         xbmc.log('This is v: {}'.format(my_videos[lenv]))
	#         addDir(vname, url.format(my_videos[lenv][v]), 3, '', vname)
		# xbmcplugin.endOfDirectory(int(sys.argv[1]))

def nextpage(name, pagenum):
	full_path = constants.search_string.format(urllib.quote(name))
	page = video.create_connection(full_path)
	videopages = video.check_amount_of_pages(page)
	for p in xrange(len(videopages)):
		if p == pagenum:
			xbmc.log('p {} equals pagenume {}'.format(p, pagenum))
			full_path = constants.youtube + videopages[p]
			xbmc.log('Full path is: {}'.format(full_path))
			page = video.create_connection(full_path)
			video.extract_title_and_link_from_next_page(page)
		# if len(video.check_amount_of_pages(page)) > 1:
			my_videos = video.extract_title_and_link(page, name)
			for v in my_videos:
				vname = v
				# xbmc.log('This is the lenv: {}'.format(lenv))
				# xbmc.log('This is v: {}'.format(my_videos[lenv]))
				addDir(vname, url.format(my_videos[v]), 3, '', vname, pagenum=pagenum)
			xbmc.log('params name is: {}'.format(params.get("name")))
			if pagenum > p:
				pass
			else:
				xbmc.log('Done, page number is {}'.format(pagenum))
				pagenum += 1
				addDir3('Next Page {} / {} >>'.format(pagenum ,len(videopages)), '', 5, '', 10, dname=dname, pagenum=pagenum)
				break


def CATAGORIES():
	addDir3('רינת', url, 2, 'http://images.mouse.co.il/storage/1/d/ggg--rinat.jpg', 10)
	addDir3('יובל המבולבל', url, 2, 'https://i.ytimg.com/vi/mPYeN0ZsRCk/maxresdefault.jpg', 10)
	addDir3('מיקי', url, 2, 'http://images.nana10.co.il/upload/mediastock/img/76/0/231/231043.jpg', 10)
	addDir3('דוד חיים', url, 2, 'http://img.mako.co.il/2013/04/17/dodhayim_c.jpg', 10)
	addDir3('בייבי אינשטיין', url, 2, 'https://yt3.ggpht.com/-aRd660Gwyzg/AAAAAAAAAAI/AAAAAAAAAAA/_1rP7GM5bvk/s900-c-k-no-rj-c0xffffff/photo.jpg', 10)
	addDir3('רוי בוי', url, 2, 'http://www.cinemall.co.il/wp-content/uploads/2016/02/royboy.jpg', 10)
	addDir3('שי ורועי', url, 2, 'http://blinker.co.il/wp-content/uploads/2014/10/kiryon.jpg', 10)
	addDir3('גילי בא לבקר', url, 2, 'http://www.nadneda.com/images/shows/gili-big.jpg', 10)
	addDir3('השלגלים', url, 2, 'http://www.ashkubit.com/wp-content/uploads/2014/05/Shalgalim-068.jpg', 10)
	addDir3('קופיקו', url, 2, 'https://i.ytimg.com/vi/G275_4-3RYM/hqdefault.jpg', 10)
	addDir3('צפצולה', url, 2, 'http://www.mesibonet.co.il/resize.ashx?path=/sites/partybook/UserContent/images/users/%D7%A6%D7%A4%D7%94%20copy_573x800(2).jpg&width=1000', 10)
	addDir3('מולי וצומי', url, 2, 'http://images.wcdn.co.il/1800849-54.jpg', 10)
	addDir3('מיכל הקטנה', url, 2, 'http://images1.ynet.co.il/PicServer3/2014/02/04/5137337/WA-shirilevi.jpg', 10)
	addDir3('אודי ואביעד', url, 2, 'http://img.mako.co.il/2010/10/19/AVIADUDIc.jpg', 10)
	addDir3('הפיגמות', url, 2, 'https://tvnewsl.files.wordpress.com/2013/03/d794d7a4d799d792d79ed795d7aa-8-9.jpg', 10)
	addDir3('יניב המגניב', url, 2, 'http://www.yomuledet.com/uploads/biz/601/yan5.jpg', 10)
	addDir3('סמי הכבאי', url, 2, 'http://www.bigshow.co.il/gallery/images/154_1254.jpg', 10)
	addDir3('לולי', url, 2, 'http://images1.ynet.co.il/PicServer2/02012008/1673158/1_wh.jpg', 10)
	addDir3('מרקו', url, 2, 'http://mscwbe.walla.co.il/w/w-300/940206-29.jpg', 10)

def PlaylistPlay(playlistid):
	xbmc.executebuiltin('XBMC.Container.Update(plugin://plugin.video.youtube/play/?playlist_id={})'.format(playlistid))


if mode == None or mode == 0:
	CATAGORIES()

if mode == 2:
	xbmc.log('Inside build video list')
	xbmc.log('The name is: {}'.format(name))
	build_video_list(name)

if mode == 3:
	xbmc.log('The play URL is {}'.format(play_url))
	xbmc.Player().play(play_url)

if mode == 5:
	xbmc.log('Page number is: {}'.format(pagenum))
	nextpage(dname, int(pagenum))




xbmcplugin.endOfDirectory(int(sys.argv[1]))
